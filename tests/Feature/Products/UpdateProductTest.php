<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_update_users_if_data_pass_validate()
    {
        $this->loginUserAdmin();
        $product = $this->createProduct();
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson(route('products.update', $product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_OK);
        $product = Product::find($product->id);
        $this->assertEquals($dataProduct['name'], $product->name);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_data_not_pass_validate()
    {
        $this->loginUserAdmin();
        $product = $this->createProduct();
        $dataProduct = Product::factory()->make([
            'name' => null
        ])->toArray();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson(route('products.update', $product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) => $json->has('errors',
            fn(AssertableJson $json) => $json->has('name')
        )->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_update_product_if_product_not_exits()
    {
        $this->loginUserAdmin();
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->put(route('products.update', -1), $dataProduct);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) => $json->has('errors')
            ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_product()
    {
        $product = $this->createProduct();
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson(route('products.update', $product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticate_user_can_not_update_product_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $product = $this->createProduct();
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->putJson(route('products.update', $product->id), $dataProduct);
        $response->assertForbidden();
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteUpdate($id): string
    {
        return route('products.update', $id);
    }

    public function makeData(): array
    {
        return Product::factory()->make()->toArray();
    }

    public function createProduct()
    {
        return Product::factory()->create();
    }

    public function makeFile(): File
    {
        Storage::disk('image');
        return UploadedFile::fake()->image(time().'.png');
    }
}
