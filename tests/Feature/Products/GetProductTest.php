<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetProductTest extends TestCase
{

    /** @test */
    public function authenticated_user_can_get_products()
    {
        $this->loginUserAdmin();
        $product = Product::factory()->create();
        $response = $this->getJson($this->getRouteShow($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) => $json->has('status_code')
            ->has('html')
            ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_get_products_if_product_not_exits()
    {
        $this->loginUserAdmin();
        $response = $this->getJson($this->getRouteShow(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) => $json->has('status_code')
            ->has('errors')
            ->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_get_product_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $product = Product::factory()->create();
        $response = $this->getJson($this->getRouteShow($product->id));
        $response->assertForbidden();
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteShow($id): string
    {
        return route('products.show', $id);
    }
}
