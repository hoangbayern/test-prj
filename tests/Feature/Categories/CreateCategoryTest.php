<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_create_category()
    {
        $this->loginUserAdmin();
        $data = $this->makeData();
        $countData = Category::count();
        $response = $this->postJson($this->getRouteStore($data));
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(fn(AssertableJson $json) => $json->where('status_code', Response::HTTP_CREATED)
            ->has('html')
            ->has('message')
            ->etc()
        );
        $this->assertDatabaseCount('categories', $countData + 1);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_category()
    {
        $data = Category::factory()->make()->toArray();
        $response = $this->postJson($this->getRouteStore($data));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticated_user_can_not_create_category_if_data_is_invalid()
    {
        $this->loginUserAdmin();
        $data = Category::factory()->make([
            'name' => null
        ])->toArray();
        $response = $this->postJson($this->getRouteStore($data));
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) => $json->has('errors',
            fn(AssertableJson $json) => $json->has('name')
                ->etc()
        )
            ->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_create_category_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $data = $this->makeData();
        $countData = Category::count();
        $response = $this->postJson($this->getRouteStore($data));
        $response->assertForbidden();
        $this->assertDatabaseCount('categories', $countData);
    }

    public function getRouteStore($data): string
    {
        return route('categories.store', $data);
    }

    public function makeData(): array
    {
        return Category::factory()->make()->toArray();
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
