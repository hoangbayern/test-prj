<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_category_if_category_exists()
    {
        $this->loginUserAdmin();
        $category = $this->createCategory();
        $countData = Category::count();
        $response = $this->deleteJson($this->getRouteDelete($category->id));
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertDatabaseMissing('categories', $category->toArray());
        $this->assertDatabaseCount('categories', $countData - 1);
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_category()
    {
        $category = $this->createCategory();
        $response = $this->deleteJson($this->getRouteDelete($category->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticated_user_can_not_delete_category_if_category_not_exists()
    {
        $this->loginUserAdmin();
        $response = $this->deleteJson($this->getRouteDelete(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) => $json->has('errors')
            ->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_delete_category_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $category = $this->createCategory();
        $countData = Category::count();
        $response = $this->deleteJson($this->getRouteDelete($category->id));
        $response->assertForbidden();
        $this->assertDatabaseCount('categories', $countData);
    }

    public function getRouteDelete($id): string
    {
        return route('categories.delete', $id);
    }

    public function createCategory()
    {
        return Category::factory()->create();
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
