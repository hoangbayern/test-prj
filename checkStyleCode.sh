#! usr/bin/bash
echo "+++++++++Check Style Code PHP - JS+++++++++"
# shellcheck disable=SC2034
PHP_CODESNIFFER=$(composer show -- squizlabs/php_codesniffer)

if [ "$PHP_CODESNIFFER" == "" ]; then
    echo "====>NO $PHP_CODESNIFFER. Install squizlabs/php_codesniffer.<===="
    composer global require "squizlabs/php_codesniffer=*" -n
fi
echo "---------->Start testing PSR-2<----------"
#./vendor/bin/phpcs --standard=PSR2 --sniffs=Generic.PHP.LowerCaseConstant tests
./vendor/bin/phpcs --standard=PSR2 app/Traits
./vendor/bin/phpcs --standard=PSR2 app/Models
./vendor/bin/phpcs --standard=PSR2 app/Services
./vendor/bin/phpcs --standard=PSR2 app/Repositories
./vendor/bin/phpcs --standard=PSR2 app/Http/Controllers
#./vendor/bin/phpcs --standard=PSR2 app/Observers
echo "---------->End testing PSR-2<----------"


ESLINT=$(npm view eslint)

if [ "$ESLINT" == "" ]; then
    echo "====>NO $ESLINT. Install eslint.<===="
    npm install --g eslint
    npm init @eslint/config
fi
echo "************>Start testing ESLINT<************"
npx eslint public/assets/js
echo "************>End testing ESLINT<************"
