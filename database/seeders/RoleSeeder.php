<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $routes = Route::getRoutes();
        $arrPrefix = [];

        foreach ($routes as $route) {
            $arrPrefix[] = $route->getPrefix();
        }

        $collectionPrefix = collect($arrPrefix)->unique()->skip(4);

        foreach ($collectionPrefix as $prefix) {
            $name = Str::replace('/', '', $prefix);
            Role::factory()->create([
                'name' => Str::ucfirst($name),
                'slug' => Str::slug($name),
                'activated' => 1,
            ]);
        }
    }
}
