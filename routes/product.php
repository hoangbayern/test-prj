<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

Route::middleware('checkActiveUser')->group(function (){
    Route::controller(ProductController::class)->group(function (){
        Route::prefix('products')->group(function (){
            Route::as('products.')->group(function (){
                Route::get('/', 'index')->name('index')
                    ->middleware('hasPermission:products,products.index');
                Route::get('/list', 'list')->name('list')
                    ->middleware('hasPermission:products,products.index');
                Route::get('/show/{id}', 'show')->name('show')
                    ->middleware('hasPermission:products,products.show');
                Route::get('/create', 'create')->name('create')
                    ->middleware('hasPermission:products,products.create');
                Route::post('/store', 'store')->name('store')
                    ->middleware('hasPermission:products,products.create');
                Route::get('edit/{id}',  'edit')->name('edit')
                    ->middleware('hasPermission:products,products.update');
                Route::put('update/{id}', 'update')->name('update')
                    ->middleware('hasPermission:products,products.update');
                Route::delete('delete/{id}', 'destroy')->name('delete')
                    ->middleware('hasPermission:products,products.delete');
            });
        });
    });
});
