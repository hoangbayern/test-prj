<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    public function model(): string
    {
        return Category::class;
    }

    public function search($name)
    {
        return $this->model->orWithName($name)->orWithParentName($name)
            ->latest('id')->paginate(config('app.paginate.limit', 10))->withQueryString();
    }

    public function list()
    {
        return $this->model->latest('id')->doesntHave('parentCategory')->get();
    }
}
