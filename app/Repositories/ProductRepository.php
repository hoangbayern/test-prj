<?php
namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    public function model(): string
    {
        return Product::class;
    }

    public function search($data)
    {
        return $this->model->withName($data['name'] ?? null)
            ->withCategoryId($data['category'] ?? null)
            ->withPrice($data['price_from'] ?? null, $data['price_to'] ?? null)
            ->latest('id')->paginate(config('app.paginate.limit', 10))->withQueryString();
    }
}
