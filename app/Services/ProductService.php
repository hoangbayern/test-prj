<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Traits\HandleImage;
use Illuminate\Http\Request;

class ProductService
{
    use HandleImage;

    public ProductRepository $productRepository;
    public CategoryRepository $categoryRepository;
    public string $pathProduct = 'products';

    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->setPath($this->pathProduct);
    }

    public function lists($request)
    {
        return $this->productRepository->search($request->all());
    }

    public function listCategories()
    {
        return $this->categoryRepository->latest('id')->get();
    }

    public function findOrFail($id)
    {
        return $this->productRepository->find($id);
    }

    public function store(Request $request)
    {
        $dataProduct = $request->except('categories');
        $dataProduct['image'] = $this->saveImage($request->file('image'));
        $categoryIds = $request->input('categoryIds');
        $product = $this->productRepository->create($dataProduct);
        $product->syncCategories($categoryIds);
        return $product;
    }

    public function update(Request $request, $id)
    {
        $product = $this->productRepository->find($id);
        $request->mergeIfMissing([
            'activated' => 0
        ]);
        $dataProduct = $request->except('categories');
        $dataProduct['image'] = $this->updateImage($request->file('image'), $product->getRawOriginal('image'));
        $categoryIds = $request->input('categoryIds');
        $product = $this->productRepository->update($dataProduct, $id);
        $product->syncCategories($categoryIds);
        return $product;
    }

    public function delete($id): bool
    {
        $product = $this->productRepository->find($id);
        $this->deleteImage($product->image);
        return $this->productRepository->delete($id);
    }
}
