<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class CategoryService
{
    public CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function list($request)
    {
        return $this->categoryRepository->search($request->name);
    }

    public function findOrFail($id)
    {
        return $this->categoryRepository->model->find($id);
    }

    public function create()
    {
        return $this->categoryRepository->list();
    }

    public function store(Request $request)
    {
        return $this->categoryRepository->create($request->all());
    }

    public function update(Request $request, $id)
    {
        $request->mergeIfMissing([
            'activated' => 0
        ]);
        return $this->categoryRepository->update($request->all(), $id);
    }

    public function delete($id): bool
    {
        return $this->categoryRepository->delete($id);
    }
}
