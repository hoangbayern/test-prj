<?php

namespace App\Services;

use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class RoleService
{
    public RoleRepository $roleRepository;
    public PermissionRepository $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function list()
    {
        return $this->roleRepository->list();
    }

    public function listPermissions()
    {
        return $this->permissionRepository->latest('id')->get();
    }

    public function store(Request $request)
    {
        $role = $this->roleRepository->create($request->except('permissions'));
        $role->permissions()->sync($request->input('permissions'));
        return $role;
    }

    public function findOrFail($id)
    {
        return $this->roleRepository->find($id);
    }

    public function update(Request $request, $id)
    {
        $request->mergeIfMissing([
            'activated' => 0
        ]);
        $role = $this->roleRepository->find($id);
        $product = $this->roleRepository->update($request->except('permissions'), $id);
        $permissionIds = $request->input('permissionIds');
        $role->syncPermissions($permissionIds);
        return $product;
    }

    public function delete($id): bool
    {
        return $this->roleRepository->delete($id);
    }
}
