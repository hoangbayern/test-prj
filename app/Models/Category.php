<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected $fillable = [
        'name',
        'slug',
        'parent_id',
        'activated'
    ];

    public static function booted()
    {
        static::creating(function ($model) {
            $model->slug = Str::slug($model->name);
        });
    }

    public function subCategories(): HasMany
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function parentCategory(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'parent_id')
            ->withDefault();
    }

    public function scopeOrWithName($query, $name)
    {
        return $name ? $query->orWhereFullText('name', $name) : $query;
    }

    public function scopeOrWithParentName($query, $name)
    {
        return $name ? $query->orWhereHas('parentCategory', fn($query) =>
            $query->whereFullText('name', $name)) : $query;
    }
}
