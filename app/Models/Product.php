<?php

namespace App\Models;

use App\Traits\HandleImage;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory, HandleImage;

    const PATH_IMAGE = 'images/products/';
    protected $table = 'products';
    protected $fillable = [
        'name',
        'slug',
        'image',
        'price',
        'stock',
        'descriptions',
        'activated'
    ];

    public static function booted()
    {
        static::creating(function ($model) {
            $model->slug = Str::slug($model->name);
        });
    }

    protected function image(): Attribute
    {
        $this->setPath(self::PATH_IMAGE);
        return Attribute::make(
            get: fn($image) => ($this->getImageFullPath($image))
        );
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'categories_products', 'product_id', 'category_id');
    }

    public function syncCategories($categoryIds): array
    {
        return $this->categories()->sync($categoryIds);
    }

    public function scopeWithCategoryId($query, $id)
    {
        return $id ? $query->whereHas('categories', function ($query) use ($id) {
            $query->where('category_id', $id);
        }) : null;
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->whereFullText('name', $name) : null;
    }

    public function scopeWithPrice($query, $priceFrom, $priceTo)
    {
        $priceFrom = $priceFrom ?: config('app.price.from');
        $priceTo = $priceTo ?: config('app.price.to');
        return $query->whereBetween('price', [$priceFrom, $priceTo]);
    }
}
