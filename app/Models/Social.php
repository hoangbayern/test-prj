<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    use HasFactory;

    protected $table = 'social_logins';
    protected $fillable = [
        'provider',
        'provider_id',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function scopeProvider($query, $name)
    {
        return $query->where('provider', $name);
    }

    public function scopeProviderId($query, $id)
    {
        return $query->where('provider_id', $id);
    }
}
