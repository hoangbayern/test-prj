<?php

namespace App\Models;

use App\Traits\HandleImage;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HandleImage;

    const ROLE_ADMIN = 'Admin';
    const PATH = 'images/users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'gender',
        'birthday',
        'avatar',
        'email',
        'phone',
        'password',
        'address',
        'activated',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function avatar(): Attribute
    {
        $this->setPath(self::PATH);
        return Attribute::make(
            get: fn($avatar) => ($this->getImageFullPath($avatar))
        );
    }

    protected function password(): Attribute
    {
        return Attribute::make(
            get: fn($password) => $password,
            set: fn($password) => bcrypt($password)
        );
    }

    protected function birthday(): Attribute
    {
        return Attribute::make(
            get: fn($birth) => (empty($birth) ? '' :
                Carbon::parse($birth)->format('d/m/Y')),
            set: fn($birth) => Carbon::createFromFormat('d/m/Y', $birth)->format('Y-m-d')
        );
    }

    public function scopeOrWithName($query, $name)
    {
        return $name ? $query->orWhereFullText('name', $name) : null;
    }

    public function scopeOrWithEmail($query, $email)
    {
        return $email ? $query->orWhereFullText('email', $email) : null;
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'roles_users', 'user_id', 'role_id');
    }

    public function syncRoles($rolesId): array
    {
        return $this->roles()->sync($rolesId);
    }

    public function hasPermission($permissionName): bool
    {
        if ($this->isAdmin()) {
            return true;
        }

        foreach ($this->roles as $role) {
            if ($role->hasPermission($permissionName)) {
                return true;
            }
        }

        return false;
    }

    public function isAdmin(): bool
    {
        return $this->hasRole(self::ROLE_ADMIN);
    }

    public function hasRole($roleName): bool
    {
        return $this->roles()->where('name', $roleName)->exists();
    }
}
