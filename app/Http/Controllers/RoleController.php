<?php

namespace App\Http\Controllers;

use App\Http\Requests\Roles\StoreRequest;
use App\Http\Requests\Roles\UpdateRequest;
use App\Repositories\RoleRepository;
use App\Services\RoleService;
use Dflydev\DotAccessData\Data;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class RoleController extends Controller
{
    protected RoleService $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $roles = $this->roleService->list();
        return view('roles.index', compact('roles'));
    }

    public function list(): Response
    {
        $roles = $this->roleService->list();
        $view = view('roles.paginate', compact('roles'))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response
    {
        $permissions = $this->roleService->listPermissions();
        $view = view('roles.create', compact('permissions'))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request): Response
    {
        $role = $this->roleService->store($request);
        $view = view('roles.show', compact('role'))->render();
        $message = 'The role has been created successfully.';
        return $this->sendResponse(Response::HTTP_CREATED, $view, $message);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id): Response
    {
        $role = $this->roleService->findOrFail($id);
        $permissions = $this->roleService->listPermissions();
        $view = view('roles.update', compact(['role', 'permissions']))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id): Response
    {
        $role = $this->roleService->update($request, $id);
        $view = view('roles.show', compact('role'))->render();
        $message = 'The role has been updated successfully.';
        return $this->sendResponse(Response::HTTP_OK, $view, $message);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): Response
    {
        $this->roleService->delete($id);
        return $this->sendResponse(Response::HTTP_NO_CONTENT);
    }
}
