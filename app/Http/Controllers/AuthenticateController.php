<?php

namespace App\Http\Controllers;

use App\Http\Requests\Authenticate\LoginRequest;
use App\Http\Requests\Authenticate\RegisterRequest;
use App\Services\UserService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Laravel\Socialite\Facades\Socialite;

class AuthenticateController extends Controller
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function viewLogin(): View|RedirectResponse
    {
        if (Auth::check()) {
            return redirect()->route('welcome');
        }
        return view('auths.login');
    }

    public function login(LoginRequest $request): RedirectResponse
    {
        $login = $this->userService->login($request);
        if ($login) {
            return redirect()->route('welcome')->withErrors([
                'success' => 'Logged in successfully.',
            ]);
        }
        return redirect()->route('auth.login')->withErrors([
            'errorLogin' => 'The email or password you entered is incorrect.',
        ]);
    }

    public function viewRegister(): View
    {
        return view('auths.register');
    }

    public function register(RegisterRequest $request): RedirectResponse
    {
        $user = $this->userService->register($request);
        if ($user) {
            Auth::loginUsingId($user->id);
            return redirect()->route('welcome')->withErrors([
                'success' => 'Logged in successfully.',
            ]);
        }
        return redirect()->back()->withErrors([
            'errorRegister' => 'Failed to register an account.',
        ]);
    }

    public function socialRedirect($provider): RedirectResponse
    {
        return Socialite::driver($provider)->redirect();
    }

    public function socialCallback($provider): RedirectResponse
    {
        $user = $this->userService->socialCallback($provider);
        if ($user) {
            Auth::loginUsingId($user->id);
            return redirect()->route('welcome')->withErrors([
                'success' => 'Logged in successfully.',
            ]);
        }
        return redirect()->route('auth.login');
    }

    public function logout(): RedirectResponse
    {
        Auth::logout();
        return redirect()->route('auth.login')->withErrors([
            'success' => 'The account has been logged out.',
        ]);
    }
}
