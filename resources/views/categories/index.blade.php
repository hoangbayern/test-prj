@extends('layouts.main')
@section('contents')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Categories Tables</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card card-outline">
                            <div class="card-header">
                                <div class="card-tools float-left">
                                    <form id="form-search" action="{{ route('categories.list') }}" method="GET"
                                          class="input-group input-group-sm" style="width: 150%;">
                                        @csrf
                                        @method('GET')
                                        <input autocomplete="off"
                                               id="name" type="text" name="name"
                                               class="form-control name" placeholder="Search category">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="card-tools float-right">
                                    <div class="input-group input-group-sm" style="width: 150%;">
                                        @hasPermission('categories', 'categories.create')
                                        <a href="{{ route('categories.create') }}"
                                           class="btn btn-sm btn-success btn-open-modal"
                                           data-toggle="modal">
                                            <i class="fas fa-plus-square mr-2"></i>
                                            <span>Create</span>
                                        </a>
                                        @endhasPermission
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <div id="table-data">

                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <div id="modal">

    </div>
@endsection
