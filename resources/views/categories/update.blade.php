<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <!-- form start -->
                            <form id="form-data" action="{{ route('categories.update', $category->id) }}" method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="parent_id">Category</label>
                                        <select class="custom-select" id="parent_id" name="parent_id">
                                            <option value="">---->Select category<----</option>
                                            @foreach($categories as $cate)
                                                <option
                                                    @selected($category->parent_id === $cate->id)  value="{{ $cate->id }}">
                                                    {{ $cate->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <span id="category-error" class="error invalid-feedback"></span>
                                    </div>
                                    <div class="form-group required">
                                        <label for="name" class="control-label">Category name:</label>
                                        <input value="{{ $category->name ?? '' }}" autocomplete="off" name="name"
                                               type="text" class="form-control" id="input-name"
                                               placeholder="Enter name">
                                    </div>

                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input @checked($category->activated === 1) name="activated"
                                                   class="custom-control-input" type="checkbox" id="activated"
                                                   value="1">
                                            <label for="activated" class="custom-control-label">Active</label>
                                            <span id="name-error" class="error invalid-feedback"></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
