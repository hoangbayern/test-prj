@php use App\Helpers\Helper; @endphp
<table class="table table-hover text-nowrap">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Slug</th>
        <th>Category</th>
        <th>Status</th>
        <th>Action(s)</th>
    </tr>
    </thead>
    <tbody>
    @if($categories->isNotEmpty())
        @foreach($categories as $category)
            <tr>
                <td>
                    {{ $category->id }}
                </td>
                <td>
                    {{ $category->name }}
                </td>
                <td>
                    {{ $category->slug }}
                </td>
                <td>
                    {{ $category->parentCategory->name }}
                </td>
                <td>
                    {!! Helper::getStatus($category->activated) !!}
                </td>
                <td>
                    @hasPermission('categories', 'categories.update')
                    <a href="{{ route('categories.edit', $category->id) }}"
                       class="btn btn-sm btn-secondary btn-open-modal" data-toggle="modal"
                       data-target="#modal-update">
                        <i class="fas fa-edit"></i>
                    </a>
                    @endhasPermission

                    @hasPermission('categories', 'categories.delete')
                    <a href="{{ route('categories.delete', $category->id) }}"
                       class="btn btn-sm btn-danger btn-delete">
                        <i class="fas fa-trash"></i>
                    </a>
                    @endhasPermission
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="6" class="text-center">
                <span style="color: red">No data found.</span>
            </td>
        </tr>
    @endif
    </tbody>
</table>
<div class="mt-3 mr-2 ml-2">
    {{ $categories->links() }}
</div>
