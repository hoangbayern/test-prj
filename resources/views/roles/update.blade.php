<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Role</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <!-- form start -->
                            <form id="form-data" action="{{ route('roles.update', $role->id) }}" method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="card-body">
                                    <div class="form-group required">
                                        <label for="name" class="control-label">Role name:</label>
                                        <input value="{{ $role->name }}" autocomplete="off" name="name" type="text"
                                               class="form-control" id="input-name" placeholder="Enter name">
                                        <span id="name-error" class="error invalid-feedback"></span>
                                    </div>
                                    <div class="form-group">
                                        <label> Permissions</label>
                                        @foreach($permissions as $permission)
                                            <div class="custom-control custom-checkbox">
                                                <input name="permissionIds[]" class="custom-control-input" type="checkbox"
                                                       id="{{ $permission->name }}" value="{{ $permission->id }}"
                                                @if($role->permissions->isNotEmpty())
                                                    @foreach($role->permissions as $per)
                                                        @checked($permission->id === $per->id)
                                                        @endforeach
                                                    @endif
                                                >
                                                <label for="{{ $permission->name }}"
                                                       class="custom-control-label">{{ $permission->name }}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input @checked($role->activated === 1) name="activated"
                                                   class="custom-control-input" type="checkbox" id="activated"
                                                   value="1">
                                            <label for="activated" class="custom-control-label">Active</label>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (left) -->
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

