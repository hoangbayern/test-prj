@php use App\Helpers\Helper; @endphp
<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail product</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <!-- Profile Image -->
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle"
                                         src="{{ $product->image }}"
                                         style="object-fit: cover; width: 250px; height: 220px;"
                                         alt="User profile picture">
                                </div>

                                <h3 class="profile-username text-center">
                                    {{ $product->name }}
                                </h3>

                                <p class="text-muted text-center">
                                    {{ $product->slug }}
                                </p>

                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Price</b>
                                        <a class="float-right">
                                            {{ number_format($product->price) }}
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Stock</b>
                                        <a class="float-right">
                                            {{ number_format($product->stock) }}
                                        </a>
                                    </li>
                                    @if($product->categories->isNotEmpty())
                                        <li class="list-group-item">
                                            <b>Categories</b>
                                            <a class="float-right">
                                                @foreach($product->categories as $category)
                                                    {!! $category->name . '<br>' !!}
                                                @endforeach
                                            </a>
                                        </li>
                                    @endif
                                    <li class="list-group-item">
                                        <b>Status</b>
                                        <a class="float-right">
                                            {!! Helper::getStatus($product->activated) !!}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                        <!-- About Me Box -->

                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-9">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">About product</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                {!! $product->descriptions !!}
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
