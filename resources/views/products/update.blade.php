@php use App\Helpers\Helper; @endphp
<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update product</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-data" action="{{ route('products.update', $product->id) }}" method="POST"
                      enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-3">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <!-- form start -->
                                <div class="card-body">
                                    <div class="d-flex justify-content-center">
                                        <div class="d-flex flex-column align-items-center">
                                            <img style="width:250px; height:250px; object-fit: cover;"
                                                 id="image-preview"
                                                 class="img-thumbnail img-circle"
                                                 src="{{ $product->image }}" alt="">
                                            <div class="form-group ml-3 mt-2">
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input class="input-image" name="image" type="file"
                                                               id="input-image" accept="image/png, image/jpeg">
                                                        <span id="image-error" class="error invalid-feedback"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                        <!-- right column -->
                        <div class="col-md-4">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <!-- form start -->
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="categories">Categories</label>
                                        <select name="categoryIds[]" class="select2" id="categories" multiple="multiple"
                                                data-placeholder="Select categories" style="width: 100%;">
                                            <option value="">---->Select categories<----</option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"
                                                @foreach($product->categories as $cat)
                                                    {{ $category->id === $cat->id ? 'selected' : '' }}
                                                    @endforeach
                                                >
                                                    {{ $category->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group required">
                                        <label for="name" class="control-label">Product name:</label>
                                        <input value="{{ $product->name }}" name="name" type="text" class="form-control"
                                               id="input-name" placeholder="Enter name">
                                        <span id="name-error" class="error invalid-feedback"></span>
                                    </div>
                                    <div class="form-group required">
                                        <label for="price" class="control-label">Product price:</label>
                                        <input value="{{ $product->price }}" name="price" type="number" min="0"
                                               class="form-control" id="input-price" placeholder="Enter price">
                                        <span id="price-error" class="error invalid-feedback"></span>
                                    </div>
                                    <div class="form-group required">
                                        <label for="stock" class="control-label">Product stock:</label>
                                        <input value="{{ $product->stock }}" name="stock" type="number" min="0"
                                               class="form-control" id="input-stock" placeholder="Enter stock">
                                        <span id="stock-error" class="error invalid-feedback"></span>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <div class="col-md-5">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <!-- form start -->
                                <div class="card-body">
                                    <div class="form-group required">
                                        <label class="description control-label">Description</label>
                                        <textarea name="descriptions" class="form-control" rows="3"
                                                  id="input-descriptions"
                                                  placeholder="Enter ...">{!! $product->descriptions !!}</textarea>
                                        <span id="descriptions-error" class="error invalid-feedback"></span>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input @checked($product->activated === 1) name="activated"
                                                   class="custom-control-input" type="checkbox" id="activated"
                                                   value="1">
                                            <label for="activated" class="custom-control-label">Active</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (right) -->
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    $('.select2').select2()
</script>
