@php use App\Helpers\Helper; @endphp
<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Product Update</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-data" action="{{ route('users.update', $user->id) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-3">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <!-- form start -->
                                <div class="card-body">
                                    <div class="d-flex justify-content-center">
                                        <div class="d-flex flex-column align-items-center">
                                            <img style="width:250px; height:250px; object-fit: cover;"
                                                 id="image-preview"
                                                 class="img-thumbnail img-circle"
                                                 src="{{ $user->avatar }}" alt="">
                                            <div class="form-group ml-3 mt-2">
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input class="input-image" name="avatar" type="file" id="avatar"
                                                               accept="image/png, image/jpeg">
                                                        <span id="avatar-error" class="error invalid-feedback"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                        <!-- right column -->
                        <div class="col-md-4">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <!-- form start -->
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="roles">Roles</label>
                                        <select name="roleIds[]" class="select2" id="input-roles" multiple="multiple"
                                                data-placeholder="Select roles" style="width: 100%;">
                                            <option value="">---->Select roles<----</option>
                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}"
                                                @foreach($user->roles as $hasRole)
                                                    {{ $hasRole->id === $role->id ? 'selected' : '' }}
                                                    @endforeach
                                                >
                                                    {{ $role->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group required">
                                        <label for="name" class="control-label">User name:</label>
                                        <input value="{{ $user->name }}" name="name" type="text" class="form-control"
                                               id="input-name" placeholder="Enter name">
                                        <span id="name-error" class="error invalid-feedback"></span>
                                    </div>
                                    <div class="form-group required">
                                        <label for="gender" class="control-label">Gender</label>
                                        <div class="row" id="input-gender">
                                            <div class="col-sm-6">
                                                <!-- checkbox -->
                                                <div class="form-group">
                                                    <div class="custom-control custom-radio">
                                                        <input @checked($user->gender === 0) name="gender" value="0"
                                                               class="custom-control-input" type="radio" id="women">
                                                        <label for="women" class="custom-control-label">Women</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <!-- radio -->
                                                <div class="form-group">
                                                    <div class="custom-control custom-radio">
                                                        <input @checked($user->gender === 1) name="gender" value="1"
                                                               class="custom-control-input" type="radio" id="men">
                                                        <label for="men" class="custom-control-label">Men</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <span id="gender-error" class="error invalid-feedback"></span>
                                    </div>

                                    <div class="form-group required">
                                        <label for="birthday" class="control-label">Birthday:</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i
                                                        class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input id="input-birthday" name="birthday" value="{{ $user->birthday }}"
                                                   type="text" class="form-control" data-inputmask-alias="datetime"
                                                   data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                                            <span id="birthday-error" class="error invalid-feedback"></span>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input name="activated"
                                                   @checked($user->activated === 1) class="custom-control-input"
                                                   type="checkbox" id="activated" value="1">
                                            <label for="activated" class="custom-control-label">Active</label>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <div class="col-md-5">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <!-- form start -->
                                <div class="card-body">
                                    <div class="form-group required">
                                        <label for="email" class="control-label">Email address</label>
                                        <input disabled name="email" value="{{ $user->email }}"
                                               type="email" class="form-control" id="input-email"
                                               placeholder="Enter email">
                                        <span id="email-error" class="error invalid-feedback"></span>
                                    </div>
                                    <div class="form-group required">
                                        <label for="phone" class="control-label">Phone</label>
                                        <input name="phone" value="{{ $user->phone }}"
                                               type="text" class="form-control" id="input-phone"
                                               placeholder="Enter phone">
                                        <span id="phone-error" class="error invalid-feedback"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input name="password"
                                               type="password" class="form-control" id="input-password"
                                               placeholder="Enter password">
                                        <span id="password-error" class="error invalid-feedback"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="address" class="description">Address</label>
                                        <textarea name="address" class="form-control" rows="3" id="input-address"
                                                  placeholder="Enter ..."></textarea>
                                        <span id="address-error" class="error invalid-feedback"></span>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (right) -->
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script src="{{ asset('assets') }}/plugins/moment/moment.min.js"></script>
<script src="{{ asset('assets') }}/plugins/inputmask/jquery.inputmask.min.js"></script>
<script>
    $(function () {
        $('.select2').select2()
        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()
    })
</script>
